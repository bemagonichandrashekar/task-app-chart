import React from 'react';
import ReactFileReader from 'react-file-reader-input';
import ChartComponent from './chartComponent'
class CSVParserComponent extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            options:{},
            csv_result:[]
        };

        this.handleFiles = this.handleFiles.bind(this);
        this.csvParse = this.csvParse.bind(this);

    }


    csvParse=(data)=>{


    const series_data =[];
        for(let data_number=0; data_number<data.length; data_number++){
           series_data.push( {
               name: data[data_number].name,
               data: data[data_number].values
           })
        }
const options_data =  {

    title: {
        text: 'Task'
    },


    xAxis: {
        title: {
            text: '<b>Years</b>'
        }
    },
    yAxis: {
        title: {
            text: '<b>Values</b>'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 1990
        }
    },

    series: series_data,

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

};

this.setState({options:options_data})
    };
    handleFiles = (e, results) => {
      const self=this;
        results.forEach(result => {

            const [e,file] = result;
            console.log(e);
            // let filename=files && files[0] && files[0].name
            let filename=file && file && file.name;
            let filetype=filename.split('.')[1];
            let fileindex=filename.indexOf('.csv');
            if(file.type === "text/csv" || filetype==='csv'|| fileindex>-1){
                let reader = new FileReader();
                reader.onload = function(event) {
                    let lines=reader.result.split("\n");
                    let resultArray = [];
                    for (let lineNumber=0; lineNumber<lines.length; lineNumber++){
                        if(lines[lineNumber]!==''){
                            let tempRow =lines[lineNumber].split(",");
                            let temSeries ={name:"",years:[],values:[]};
                            temSeries.name= tempRow[0];
                            for(let temSeriesNumebr=1;temSeriesNumebr<tempRow.length;temSeriesNumebr++){
                                const seriesString = tempRow[temSeriesNumebr];
                                const seriesArray = seriesString.split('|');
                                temSeries.years.push(seriesArray[0]);
                                temSeries.values.push(parseInt(seriesArray[1]));
                            }
                            resultArray.push(temSeries);
                        }

                    }
 self.setState({csv_result:resultArray});
                    self.csvParse(resultArray);
                    console.log(resultArray,"resultArray")


                }
                reader.readAsText(file);

            }else{
              alert("File not supported")
            }
        });
    }
    render() {

        debugger
        return <div> <ReactFileReader onChange={this.handleFiles} accept=".csv" fileTypes={['.csv']} multipleFiles={true}>
            <button className='btn btn-upload-config'>Upload</button>
            <div style={{'font-size': '10px'}}> (CSV file only) </div>
        </ReactFileReader>

            <ChartComponent options_data1 ="dd" options_data={JSON.stringify(this.state.options)}/></div>
    }
}

export  default CSVParserComponent;